const {zazaDb} = require('../models');

const controller = {
    create: async (req, res) => {
        const payload = {
            nume:req.body.nume,
            contact:req.body.contact,
            apelativ:req.body.apelativ,
            nrDoze:req.body.nrDoze,
            undeAuzit:req.body.recomandare,
        }

        try {
            if (payload.nume == null || payload.contact == null || payload.apelativ == null || payload.nrDoze == null || payload.undeAuzit == null) {
                return res.status(400).send("Campurile nu au fost completate!");
            }
            const newZaza = await zazaDb.create(payload);
            res.status(200).send(newZaza);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}

module.exports = controller