const Sequelize = require('sequelize');

const database = new Sequelize(
    'devops_challenge_2024',
    'user_nebun',
    'Devops1_1Challange',
    {
        dialect: "mysql",
        host: "localhost",
        dialectOptions: {
            charset: "utf8",
            collate: "utf8_general_ci",
        },
        define: {
            timestamps: true,
        },
    },
);

// const database = new Sequelize(
//     'devops_challenge_2024',
//     'root',
//     '',
//     {
//         dialect: "mysql",
//         host: "localhost",
//         dialectOptions: {
//             charset: "utf8",
//             collate: "utf8_general_ci",
//         },
//         define: {
//             timestamps: true,
//         },
//     },
// );

module.exports = database;