module.exports = (database, sequelize) => {
    return database.define('zaza', {
        id: {
            type: sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        nume: {
            type: sequelize.STRING,
            required: true
        },
        contact: {
            type: sequelize.STRING,
            required: true
        },
        apelativ: {
            type: sequelize.STRING,
            required: true
        },
        nrDoze: {
            type: sequelize.INTEGER,
            required: true
        },
        undeAuzit: {
            type: sequelize.STRING,
            required: true
        },
    });
};
