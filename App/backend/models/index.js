const sequelize = require("sequelize");
const database = require("../config/database");
const zazaModel = require('./zaza');

const zazaDb = zazaModel(database, sequelize);

module.exports = {
    zazaDb,
    database
}
