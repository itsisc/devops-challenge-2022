const router = require('express').Router();

const {zazaController} = require('../controllers');

router.post('/create', zazaController.create);

module.exports = router;
