const zazaRouter = require("./zaza");
const router = require('express').Router();

router.use('/zaza', zazaRouter);

module.exports = router;