import {
    Box,
    Button,
    Flex,
    Image,
    keyframes, Link,
    Popover,
    PopoverArrow, PopoverBody, PopoverCloseButton,
    PopoverContent, PopoverHeader,
    PopoverTrigger,
    Text
} from "@chakra-ui/react";
import {useNavigate} from "react-router-dom";

const rainbowAnimation = keyframes`
    from {
        color: #6666ff;
    }
    10% {
        color: #0099ff;
    }
    50% {
        color: #00ff00;
    }
    75% {
        color: #ff3399;
    }
    100% {
        color: #6666ff;
    }
`;

export const Home = () => {
    const navigate = useNavigate();
    return (
        <Flex
            flexDirection="column"
            alignItems="center"
            userSelect="none"
            height="100%"
            width="100%"
            gap="100px"
            p="5%"
        >
            <Flex
                flexDirection="column"
                justifyContent="center"
                alignItems="center"
            >
                <Text fontSize="80px">Berea <Text as="span" color="green">zaza</Text><Text as="span"
                                                                                           verticalAlign="text-top"
                                                                                           fontSize="30px">(™)</Text></Text>
                <Text fontSize="35px">{'"Cine știe, cunoaște!"'}</Text>
            </Flex>
            <Flex
                width="80%"
                height="400px"
                gap="20px"
            >
                <Flex
                    flex={0.5}
                    flexDirection="column"
                    alignItems="center"
                    textAlign="justify"
                >
                    <Text fontSize="27px">
                        &emsp;&emsp;Fiecare înghițitură din berea Zaza dezvăluie o poveste adâncă și mistică, încorporată
                        în esența sa unică, Zaza. Această băutură nu doar răsfață simțurile, ci și răspândește o energie
                        autentică și luminată. Cu Zaza, vei descoperi o lume de savoare autentică, departe de orice
                        umbre sau întrebări neclare. Și da, odată ce ai ales Zaza, nu ai nevoie de garanții -
                        satisfacția este garantată.
                    </Text>
                </Flex>
                <Flex
                    position="relative"
                    flex={0.5}
                    flexDirection="column"
                >
                    <Popover>
                        <PopoverTrigger>
                            <Box
                                position="absolute"
                                top="50%"
                                transform="translateY(-50%)"
                                right="0"
                                height="100px"
                                width="100px"
                                cursor="pointer"
                            />
                        </PopoverTrigger>
                        <PopoverContent>
                            <PopoverArrow />
                            <PopoverCloseButton />
                            <PopoverHeader>Ai câștigat zaza de aur!</PopoverHeader>
                            {/* <PopoverBody>Iți poți revendica premiul de <Link href="https://www.youtube.com/watch?v=biYGBhcYOCY&pp=ygUnbGVmdCBlYXIgbW96YXJ0IHJpZ2h0IGVhciBjaGluZXNlIGJpYmxl" isExternal>aici!</Link></PopoverBody> */}
                            <PopoverBody>Revino aici mai târziu tho, dar dacă tot ești ACUM aici, uite <Link href="https://docs.google.com/document/d/1YrxkwCMTy9bmJvO6mBHZEQCYLov3D88BOy7cvu8TEds/edit?usp=sharing" isExternal>asta!</Link></PopoverBody>
                        </PopoverContent>
                    </Popover>

                    <Image width="100%" height="100%" src="assets/bere_zaza.jpeg" userSelect="none"/>
                </Flex>
            </Flex>
            <Flex
                flexDirection="column"
                justifyContent="center"
                alignItems="center"
                gap="10px"
            >
                <Text fontSize="40px">Ce mai aștepți, comandă acum!</Text>
                <Button colorScheme="green" onClick={() => navigate('order')}>Plasează comanda</Button>
            </Flex>
        </Flex>
    );
}
