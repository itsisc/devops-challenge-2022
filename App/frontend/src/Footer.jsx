import {Flex, Text} from "@chakra-ui/react";

export const Footer = () => {
    return (
        <Flex
            width="100%"
            height="7vh"
            bgColor="grey"
            justifyContent="center"
            alignItems="center"
            userSelect="none"
        >
            <Text fontStyle="italic">© 2024 Berea zaza(™)</Text>
        </Flex>
    );
}
