import {
    Button, Flex,
    Image,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalHeader,
    ModalOverlay, Text, useToast
} from "@chakra-ui/react";
import axios from "axios";
import {useEffect, useState} from "react";

const OrderModalContent = ({onClose, order}) => {
    const toast = useToast();

    const [nrOfClicks, setNrOfClicks] = useState(0);
    const [hasTimerEnded, setHasTimerEnded] = useState(false);
    const [hasFinishedClicking, setHasFinishedClicking] = useState(false);


    useEffect(() => {
        const timeout = setTimeout(() => {
            setHasTimerEnded(true);
            setHasFinishedClicking(true);
        }, 10000);

        return () => clearTimeout(timeout);
    }, []);

    useEffect(() => {
        if (hasTimerEnded && nrOfClicks < 50) {
            toast({
                title: 'Eroare!',
                description: 'Nu ai spamat suficient berea zaza! Incearcă din nou!',
                status: 'error',
                duration: 2000,
                isClosable: true,
            });
            setHasFinishedClicking(true);
            onClose();
        } else if (nrOfClicks >= 50) {
            axios
            .post("http://localhost:4004/api/zaza/create",order)
            .then((res)=>{
                toast({
                    title: 'Succes!',
                    description: 'Felicitări! Vei deveni cât de curând un nou deținător de bere zaza!',
                    status: 'success',
                    duration: 5000,
                    isClosable: true,
                });
            })
            .catch((err)=>{
                toast({
                    title: 'Eroare!',
                    description: 'eroare... mai incearca o data!',
                    status: 'error',
                    duration: 2000,
                    isClosable: true,
                });
            })
            .finally(()=>{
                setHasFinishedClicking(true);
                onClose();
            });
        }
    }, [nrOfClicks, hasTimerEnded, onClose, toast]);

    return (
        <ModalContent>
            <ModalHeader>Spamează berea zaza!</ModalHeader>
            <ModalCloseButton/>
            <ModalBody
                display="flex"
                justifyContent="center"
                alignItems="center"
                flexDirection="column"
                textAlign="center"
            >
                <Text>Ai 10 secunde să apeși de 50 de ori pe bere pentru a plasa comanda!</Text>
                <Text>0:10</Text>
                <Text fontSize="25px">{nrOfClicks}</Text>
                <Button
                    position="relative"
                    bgColor="transparent"
                    outline="none"
                    isDisabled={hasFinishedClicking}
                    onClick={() => setNrOfClicks((prevState) => prevState + 1)}
                    display="flex"
                    width="200px"
                    height="200px"
                >
                    <Image src="assets/bere_zaza.jpeg" borderRadius="7px"/>
                </Button>
            </ModalBody>
        </ModalContent>
    )
}

export const OrderModal = ({isOpen, onClose, order}) => {
    return (
        <Modal isOpen={isOpen} onClose={onClose} isCentered>
            <ModalOverlay/>
            <OrderModalContent onClose={onClose} order={order}/>
        </Modal>
    )
}