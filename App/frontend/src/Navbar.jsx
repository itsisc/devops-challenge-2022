import {Box, Button, Flex, Image} from "@chakra-ui/react";
import {useNavigate} from "react-router-dom";

export const Navbar = () => {
    const navigate = useNavigate();

    return (
        <Flex
            position="relative"
            width="100%"
            height="7vh"
            borderBottomLeftRadius="15px"
            borderBottomRightRadius="15px"
            boxShadow="dark-lg"
            justifyContent="space-between"
            alignItems="center"
            px="20px"
            py="10px"
        >
            <Box
                height="100%"
                aspectRatio={1}
                bgColor="green"
                onClick={() => navigate('')}
                cursor="pointer"
            >
                <Image src="assets/bere_zaza.jpeg" />
            </Box>
            <Flex
                height="100%"
                justify="center"
                align="center"
            >
                <Button colorScheme="teal" onClick={() => navigate('contact')}>Contact</Button>
            </Flex>
        </Flex>
    );
}
