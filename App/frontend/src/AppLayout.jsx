import {Box, Flex} from "@chakra-ui/react";
import {Outlet} from "react-router-dom";
import {Navbar} from "./Navbar.jsx";
import {Footer} from "./Footer.jsx";

export const AppLayout = () => {
    return (
        <Flex width="100vw" minHeight="100vh" alignItems="center" flexDirection="column">
            <Navbar/>
            <Box p={0} m={0} flex={1} width="100%">
                <Outlet/>
            </Box>
            <Footer />
        </Flex>

    )
}