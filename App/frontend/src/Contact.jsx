import {Box, Flex, Grid, GridItem, Image, SimpleGrid, Text} from "@chakra-ui/react";


const CONTACT_DATA = [
    {picUrl: "assets/ceascaNou.jpg", contact: "@mircea_ceasca", link: "https://youtu.be/c-dqkLR6JEA?list=PL6KIXDHUuyPC42BbKymTrufyD9x2LIEjr", },
    {picUrl: "assets/cosminNou.jpg", contact: "@cosminangeles", link: "https://www.youtube.com/watch?v=fo6T5BwxFh0", },
    {picUrl: "assets/cristi_rece.jpeg", contact: "@miqi.the.rooster", link: "https://www.youtube.com/watch?v=rscFghE4g3k", },
    {picUrl: "assets/dani.jpeg", contact: "@danieldanila_", link: "https://www.youtube.com/watch?v=D-U0mbT-jkw", },
    {picUrl: "assets/dudu.jpg", contact: "@vlad.dudu13", link: "https://www.youtube.com/shorts/WgY5Q5d9SNQ", },
    {picUrl: "assets/irina.jpeg", contact: "@irinaaa.25", link: "https://youtu.be/TcePkwagNFA?si=sckGXJTHwcZFQlhC", },
    {picUrl: "assets/margi.jpeg", contact: "@_____margi_____", link: "https://www.youtube.com/watch?v=LaGP-CVfENQ", },
    {picUrl: "assets/mazi.jpeg", contact: "@mazilu_mircea", link: "https://youtu.be/bvmBCa2RORQ", },
    {picUrl: "assets/munti.jpg", contact: "@cris_munti", link: "https://www.youtube.com/watch?v=uHQ1w7XckNo", },
    {picUrl: "assets/rares.jpeg", contact: "@viv_rares", link: "https://www.youtube.com/watch?v=wP-DzVvhdms", },
    {picUrl: "assets/sendrea.jpeg", contact: "@andrei_schender", link: "https://www.youtube.com/watch?v=tGkmKRN5P4U&t=324s", },
    {picUrl: "assets/Geo.jpeg", contact: "@georgiana__gavril", link: "https://www.youtube.com/watch?v=bm0nLJuRNbw", },
    {picUrl: "assets/Ana.jpeg", contact: "@anaachim_888", link: "https://www.youtube.com/watch?v=GAKD9g9VRt0", },
]

export const Contact = () => {
    return (
        <Flex
            width="100%"
            height="100%"
            alignItems="center"
            flexDirection="column"
            py="50px"
            gap="100px"
        >
            <Flex
                width="70%"
                flexDirection="column"
                alignItems="center"
                textAlign="center"
                gap="20px"
            >
                <Text fontSize="30px">Vrei să ne transmiți o vorbă bună? Ai întâmpinat o problema cu berea ta zaza sau
                    nu are gustul pe care ți l-ai dorit?</Text>
                <Text fontSize="30px">Uite echipa noastră, dă-ne un mesaj si vom lua imediat legatura!</Text>
            </Flex>

            <Flex
                justifyContent="center"
                alignItems="center"
                flexWrap="wrap"
                width="70%"
                gap="10px"
            >
                {CONTACT_DATA.map((data, index) => (
                    <a key={index} href={data.link} target="_blank">
                        <Box
                            width="150px"
                            height="300px"
                            textAlign="center"
                        >
                            <Image src={data.picUrl}/>
                            <Text display="inline-block">{data.contact}</Text>
                        </Box>
                    </a>
                ))}
            </Flex>
        </Flex>
    );
}
