import {createBrowserRouter, createRoutesFromElements, Route, RouterProvider} from "react-router-dom";
import {AppLayout} from "./AppLayout.jsx";
import {Home} from "./Home.jsx";
import {Order} from "./Order.jsx";
import {Contact} from "./Contact.jsx";

const router = createBrowserRouter(
    createRoutesFromElements(
        <>
            <Route path="/" element={<AppLayout/>}>
                <Route index element={<Home/>}/>
                <Route path="order" element={<Order/>}/>
                <Route path="contact" element={<Contact/>}/>
            </Route>
        </>
    ),
    {
        basename: '/second/',
    }
);

export const Navigator = () => (
    <RouterProvider router={router}/>
)
