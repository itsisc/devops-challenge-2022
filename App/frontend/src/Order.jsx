import {Box, Button, Flex, Input, NumberInput, NumberInputField, Text, useDisclosure, useToast} from "@chakra-ui/react";
import {useCallback, useEffect, useState} from "react";
import {OrderModal} from "./OrderModal.jsx";

export const Order = () => {
    const toast = useToast();

    const {isOpen, onOpen, onClose} = useDisclosure();

    const [nume, setNume] = useState('');
    const [contact, setContact] = useState('');
    const [apelativ, setApelativ] = useState('');
    const [nrDoze, setNrDoze] = useState('');
    const [recomandare, setRecomandare] = useState('');
    const [order,setOrder] = useState(null);

    const onSubmit = useCallback((e) => {
        e.preventDefault();

        if (!(nume && contact && apelativ && nrDoze && recomandare)) {
            toast({
                title: 'Eroare!',
                description: 'Unul sau mai multe câmpuri nu au fost completate!',
                status: 'error',
                duration: 5000,
                isClosable: true,
            })
            return;
        }
        const oderData = {
            nume,
            contact,
            apelativ,
            nrDoze,
            recomandare
        }
        setOrder(oderData);
        onOpen();
    }, [nume, contact, apelativ, nrDoze, recomandare, onOpen, toast]);

    useEffect(() => {

    }, [isOpen]);

    return (
        <Flex
            width="100%"
            height="100%"
            alignItems="center"
            flexDirection="column"
            py="50px"
        >
            <Flex
                width="30%"
                justifyContent="center"
                flexDirection="column"
                gap="20px"
                as="form"
                onSubmit={onSubmit}
            >
                <Flex
                    width="100%"
                    flexDirection="column"
                    justifyContent="center"
                >
                    <Text>Nume complet</Text>
                    <Input placeholder="ex. Gigel Gogulescu" value={nume} onChange={(e) => setNume(e.target.value)}/>
                </Flex>

                <Flex
                    width="100%"
                    flexDirection="column"
                    justifyContent="center"
                >
                    <Text>O metodă de contact (email, telefon, cod poștal etc.)</Text>
                    <Input placeholder="Credem că te descurci :)" value={contact} onChange={(e) => setContact(e.target.value)}/>
                </Flex>

                <Flex
                    width="100%"
                    flexDirection="column"
                    justifyContent="center"
                >
                    <Text>Apelativ AKA. cum ți se spune an cartier</Text>
                    <Input placeholder="ex. Soarele meu viața mea" value={apelativ} onChange={(e) => setApelativ(e.target.value)}/>
                </Flex>

                <Flex
                    width="100%"
                    flexDirection="column"
                    justifyContent="center"
                >
                    <Text>Numărul de doze pe care ți le dorești</Text>
                    <NumberInput
                        min={0}
                        value={nrDoze}
                        onChange={(value) => setNrDoze(parseInt(value))}
                    >
                        <NumberInputField placeholder="ex. 1923812e-10312" />
                    </NumberInput>
                </Flex>

                <Flex
                    width="100%"
                    flexDirection="column"
                    justifyContent="center"
                >
                    <Text>De unde ai auzit de
                        <Text as="span"> Berea
                            <Text as="span" color="green"> zaza</Text>
                            <Text as="span" verticalAlign="text-top" fontSize="10px">(™)</Text>
                        </Text>
                        ?
                    </Text>
                    <Input placeholder="ex. De la prietenului nașului iubitului soției mele" value={recomandare} onChange={(e) => setRecomandare(e.target.value)}/>
                </Flex>

                <Button colorScheme="green" type="submit">Comandă</Button>
            </Flex>
            <OrderModal isOpen={isOpen} onClose={onClose} order={order} />
        </Flex>
    );
}
