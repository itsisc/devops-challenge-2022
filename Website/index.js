const AD_DATA = [
    {
        pictureUrl: "assets/reclama_cosmin.jpeg",
        aspectRatio: 1.5,
    },
    {
        pictureUrl: "assets/reclama_ceasca.jpeg",
        aspectRatio: 1.5,
    },    {
        pictureUrl: "assets/reclama_cristi_rece.jpeg",
        aspectRatio: 1.5,
    },    {
        pictureUrl: "assets/reclama_visan.jpeg",
        aspectRatio: 1.5,
    },
];


window.addEventListener('DOMContentLoaded', () => {
    const copyrightText = document.getElementById('copyright-text');
    copyrightText.onclick = () => {
        const a = document.createElement('a');
        a.href = 'https://www.youtube.com/watch?v=Amym5jnB1vc';
        a.target = '_blank';
        a.click();
    }


    let zIndex = 1;
    setInterval(() => {
        const adId = Math.floor(Math.random() * AD_DATA.length);
        const adData = AD_DATA[adId];

        const adDiv = document.createElement('div');
        const adCloseButton = document.createElement('button');
        adCloseButton.classList.add('close-ad-button');

        adDiv.style.position = 'absolute';
        adDiv.style.backgroundImage = `url("${adData.pictureUrl}")`;
        adDiv.style.backgroundSize = 'cover';
        adDiv.style.width = '200px';
        adDiv.style.aspectRatio = adData.aspectRatio;
        adDiv.style.zIndex = `${zIndex++}`;

        adDiv.style.top = `${Math.floor(Math.random() * document.body.scrollHeight)}px`;
        adDiv.style.left = `${Math.floor(Math.random() * document.body.scrollWidth)}px`;

        adCloseButton.onclick = () => {
            document.body.removeChild(adDiv);
        }

        adDiv.append(adCloseButton);

        document.body.append(adDiv);
    }, 5000);
});